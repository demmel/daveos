daveOS -- An operating system by Dave
=====================================
This is my first shot at developing an OS from the ground up.  I've wanted to
get started on this, but never got around to it.  I'm also a big fan of the
D programming language, so I'd like to do this in D even though the barebones
pieces of the language are pretty minimal.  It might help me be able to support
barebones D if I have experience with it.

I have no real plan at the moment as to where I want to take this.  I'm
figuring it out as I go and refactoring along the way.  I think I may
either take it in the direction of an expandable microkernel or exokernel
with much functionality presented through modules and userland libraries

Buildiing
---------
At the moment, the OS can only be compiled for i586 as an ELF loadable by
grub.  You will need a cross compiler for i586-elf.  The Makefile is written
for use with gdc, but you can modify it as needed for whatever compiler you
use, just make sure it's in your path.

You will need grub if you want the makefile to generate an ISO image with grub
and the OS on it.

`make` will build the system binary and create an ISO img with grub and the OS
on it.

`make daveOS.bin` will build just the system binary.

Running
-------
Using `make run` you can run the in the qemu emulator.  There may be
configurations for choosing an emulator or even a run enivronment setup later.
