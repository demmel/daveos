OSNAME := daveOS
ARCH=i586
PREFIX=$(ARCH)-elf-
CC=$(PREFIX)gdc
AS=$(PREFIX)as

SRCDIR := src
OBJDIR := obj

D_SRCS := $(filter-out $(SRCDIR)/arch/%, $(shell find $(SRCDIR)/ -type f -name '*.d'))
D_SRCS += $(shell find $(SRCDIR)/arch/$(ARCH)/ -type f -name '*.d')
AS_SRCS := $(shell find $(SRCDIR)/arch/$(ARCH)/asm/ -type f -name '*.s')

D_OBJS := $(patsubst $(SRCDIR)/%.d,$(OBJDIR)/%.o,$(D_SRCS))
AS_OBJS := $(patsubst $(SRCDIR)/arch/$(ARCH)/asm/%.s,$(OBJDIR)/arch/$(ARCH)/asm/%.o,$(AS_SRCS))
OBJS := $(AS_OBJS) $(D_OBJS)

CFLAGS := -nostdlib -nodefaultlibs -fno-emit-moduleinfo -g -Isrc/druntime
LFLAGS := -ffreestanding -O2 -nostdlib -lgcc

.PHONY: all
all: $(OSNAME).iso

.PHONY: run
run: $(OSNAME).iso
	@echo "RUN\t$(OSNAME)"
	@qemu-system-i386 -cdrom $(OSNAME).iso

$(OSNAME).iso: $(OSNAME).bin
	@echo "ISO\t$(OSNAME).iso"
	@mkdir -p $(OBJDIR)/isodir/boot/grub/
	@cp $(OSNAME).bin $(OBJDIR)/isodir/boot
	@cp grub.cfg $(OBJDIR)/isodir/boot/grub
	@grub-mkrescue -o $(OSNAME).iso $(OBJDIR)/isodir

$(OSNAME).bin: $(OBJS) linker.ld
	@echo "LD\t$(OSNAME).bin"
	@$(CC) $(LFLAGS) -T linker.ld -o $(OSNAME).bin $(AS_OBJS) $(D_OBJS)

$(OBJDIR)/%.o: $(SRCDIR)/%.d
	@echo "CC\t$<"
	@mkdir -p "$(@D)"
	@$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)/arch/$(ARCH)/asm/%.o: $(SRCDIR)/arch/$(ARCH)/asm/%.s
	@echo "AS\t$<"
	@mkdir -p "$(@D)"
	@$(AS) $< -o $@

.PHONY: clean
clean:
	@echo "RM\tobject and output files"
	@rm -rf $(OBJDIR)/ *.iso *.bin
