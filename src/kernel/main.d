module kernel.main;

extern(C) __gshared void* _Dmodule_ref;

extern(C) void main(uint magic, uint addr) {
	int ypos = 0;
	int xpos = 0;
	const uint COLUMNS = 80;
	const uint LINES = 25;

	ubyte* vidmem = cast(ubyte*)0xFFFF8000000B8000;

	for (int i = 0; i < COLUMNS * LINES * 2; i++) {
		*(vidmem + i) = 0;
	}

	*(vidmem + (xpos + ypos * COLUMNS) * 2) = 'D' & 0xFF;
	*(vidmem + (xpos + ypos * COLUMNS) * 2 + 1) = 0x07;

	while(true) { }
}
