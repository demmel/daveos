.global start
.extern main       # Allow main() to be called from the assembly code
.extern start_ctors
.extern end_ctors
.extern start_dtors
.extern end_dtors

.equ MODULEALIGN, 1<<0
.equ MEMINFO,     1<<1
.equ FLAGS,       MODULEALIGN | MEMINFO
.equ MAGIC,       0x1BADB002
.equ CHECKSUM,    -(MAGIC + FLAGS)

.text              # Next is the Grub Multiboot Header

.equ STACKSIZE, 0x4000

.align 4
MultiBootHeader:
	.long MAGIC
	.long FLAGS
	.long CHECKSUM

static_ctors_loop:
	movl $start_ctors, %ebx
	jmp static_ctors_loop.test
static_ctors_loop.body:
	call *(%ebx)
	addl $4,%ebx
static_ctors_loop.test:
	cmpl $end_ctors, %ebx
	jb static_ctors_loop.body

start:
	movl $STACKSIZE+stack, %esp

	pushl %eax
	pushl %ebx

	call main

static_dtors_loop:
	movl $start_dtors, %ebx
	jmp static_dtors_loop.test
static_dtors_loop.body:
	call *(%ebx)
	addl $4,%ebx
static_dtors_loop.test:
	cmpl $end_dtors, %ebx
	jb static_dtors_loop.body

cpuhalt:
	hlt
	jmp cpuhalt

.bss
.align 32

.lcomm stack, STACKSIZE

