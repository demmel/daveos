#!/bin/bash

# Simple script to download and build the tools needed to build
# daveOS.  It's currently setup to work the way I've got things
# on my machine, but you could edit the TARGET amd PREFIX to
# install the way that meets your needs.  Just remember the tools
# need to be in your PATH.  If you change the TARGET make sure to
# also edit the Makefile.  Hopefully I'll automate this a bit in
# the future.

set -e

unset CPATH LIBRARY_PATH C_INCLUDE_PATH CPLUS_INCLUDE_PATH

export TARGET=i586-elf
export PREFIX=/usr/local/cross

mkdir -p $PREFIX

./get-binutils.sh
./get-gdc.sh

echo "Finished downloading and building tools"
echo "If binutils was built then please run 'make install' in the build directory"
echo "If gdc was built then please run 'make install-gcc' and"
echo "'make install-target-libgcc' in the build directory"
