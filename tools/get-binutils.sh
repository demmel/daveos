#!/bin/bash

set -e

if [ -e "$PREFIX/bin/$TARGET-as" ]
then
	echo "$TARGET-as exists.  Skipping binutils"
	exit 0
fi

# Delete existing binutils source archive and download a new one
#-------------------------------------------------------------------
export BINUTILS_NAME=binutils-2.24
export BINUTILS_SOURCE_ARCHIVE=$BINUTILS_NAME.tar.bz2
rm -f $BINUTILS_SOURCE_ARCHIVE
rm -rf $BINUTILS_NAME
wget http://ftpmirror.gnu.org/binutils/$BINUTILS_SOURCE_ARCHIVE

# Extract binutils
#-------------------------------------------------------------------
tar xjfv $BINUTILS_SOURCE_ARCHIVE

# Create binutils build directory
#-------------------------------------------------------------------
export BINUTILS_BUILD_DIR=binutils-build
rm -rf $BINUTILS_BUILD_DIR
mkdir $BINUTILS_BUILD_DIR

# Configure and build binutils
#-------------------------------------------------------------------
cd $BINUTILS_BUILD_DIR
../$BINUTILS_NAME/configure \
	--target=$TARGET   \
	--prefix=$PREFIX   \
	--disable-nls      \
	--disable-multilib \
	--with-gnu-as      \
	--with-gnu-ld      \
	--disable-libssp   \
	--disable-werror
make -j4 all


