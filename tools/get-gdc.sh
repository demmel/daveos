#!/bin/bash

set -e

if [ -e "$PREFIX/bin/$TARGET-gdc" ]
then
	echo "$TARGET-gdc exists.  Skipping gcc/gdc"
	exit 0
fi

# Delete existing GCC source archive and download a new one
#-------------------------------------------------------------------
export GCC_NAME=gcc-4.8.2
export GCC_SOURCE_ARCHIVE=$GCC_NAME.tar.bz2
rm -f $GCC_SOURCE_ARCHIVE
rm -rf $GCC_NAME

wget ftp://ftp.gnu.org/gnu/gcc/$GCC_NAME/$GCC_SOURCE_ARCHIVE

# Extract GCC
#-------------------------------------------------------------------
tar xjfv $GCC_SOURCE_ARCHIVE

# Download GDC
#-------------------------------------------------------------------
rm -rf gdc
mkdir gdc
git clone https://github.com/D-Programming-GDC/GDC.git gdc
cd gdc
git checkout gdc-4.8
./setup-gcc.sh ../$GCC_NAME
cd ..

# Create GCC build directory
#-------------------------------------------------------------------
export GCC_BUILD_DIR=gcc-build
rm -rf $GCC_BUILD_DIR
mkdir $GCC_BUILD_DIR

# Configure and build GCC
#-------------------------------------------------------------------
cd $GCC_BUILD_DIR
../$GCC_NAME/configure --target=$TARGET --prefix=$PREFIX \
	--enable-languages=d     \
	--disable-bootstrap      \
	--disable-libssp         \
	--disable-libgomp        \
	--disable-libmudflap     \
	--disable-multilib       \
	--disable-libphobos      \
	--disable-decimal-float  \
	--disable-libffi         \
	--disable-libmudflap     \
	--disable-libquadmath    \
	--disable-libssp         \
	--disable-libstdcxx-pch  \
	--disable-nls            \
	--disable-shared         \
	--disable-threads        \
	--disable-tls            \
	--with-gnu-as            \
	--with-gnu-ld            \
	--without-headers
make -j4 all-gcc
make -j4 all-target-libgcc
